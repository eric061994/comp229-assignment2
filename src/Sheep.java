import java.awt.*;
import java.io.File;
import java.lang.*;
import java.util.ArrayList;
import java.util.Optional;
import java.util.List;
import java.util.function.Supplier;

import bos.MoveRandomly;
import bos.NoMove;
import bos.RelativeMove;

import javax.imageio.ImageIO;

/**
 * Created by 44942117 on 22/08/2017.
 */
public class Sheep extends Character {

    public Sheep(Cell location, Supplier<RelativeMove> behaviour){
        super(location, behaviour);
        try{
            display = Optional.of(ImageIO.read(new File("sheep.png")));
        } catch (Exception e) {
            display = Optional.empty();
        }
        type = Optional.of("Sheep");
        movement = 3;
        movesLeft = 3;
    }


}
