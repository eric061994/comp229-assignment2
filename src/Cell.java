import java.awt.*;
import java.util.Optional;

/**
 * Created by 44942117 on 15/08/2017.
 */
public class Cell extends Rectangle {

    //A variable that allows the program to recognise the type of terrain
    Optional<String> terrain;

    public Cell(int x, int y){
        super(x,y,35,35);
        terrain = Optional.empty();
    }

    public void paint (Graphics g, Boolean highlighted){

        g.fillRect(x,y,35, 35);
        g.setColor(Color.BLACK);
        g.drawRect(x,y, 35, 35);

        if (highlighted) {
            g.drawRect(x+1, y+1, 33,33);
        }
    }

    @Override
    public boolean contains(Point target){
        if(target == null)
            return false;
        else{
            return super.contains(target);
        }
    }
}
