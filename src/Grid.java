import bos.*;

import java.util.function.Consumer;
import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.function.Predicate;

/**
 * Created by 44942117 on 15/08/2017.
 */
public class Grid implements bos.GameBoard<Cell> {

    private Cell[][] grid = new Cell[20][20];
    private int[][] terrain = new int[20][20];

    private int x;
    private int y;

    public Grid(int x, int y) {
        this.x = x;
        this.y = y;

        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                if(i < 7 && j < 7){
                    grid[i][j] = new Rocks(x + j * 35, y + i * 35);
                    terrain[i][j] = 0;
                } else if(i > 14 && j < 7){
                    grid[i][j] = new DarkTree(x + j * 35, y + i * 35);
                    terrain[i][j] = 0;
                }else if(j > 16){
                    grid[i][j] = new Dirt(x + j * 35, y + i * 35);
                    terrain[i][j] = 2;
                }else if(i > 14 && j > 10){
                    grid[i][j] = new DarkTree(x + j * 35, y + i * 35);
                    terrain[i][j] = 0;
                } else {
                    grid[i][j] = new LightGrass(x + j * 35, y + i * 35);
                    terrain[i][j] = 1;
                }
            }
        }


    }

    public void paint(Graphics g, Point mousePosition) {

        dotoEachCell((c -> c.paint(g, c.contains(mousePosition))));
    }

    public Cell getRandomCell() {
        java.util.Random rand = new java.util.Random();
        return grid[rand.nextInt(20)][rand.nextInt(20)];
    }

    public Cell getCell(int a, int b){
        return grid[a][b];
    }

    private Pair<Integer, Integer> indexOfCell(Cell c) {
        for (int x = 0; x < 20; x++) {
            for (int y = 0; y < 20; y++) {
                if (grid[x][y] == c) {
                    return new bos.Pair(x, y);
                }
            }
        }
        return null;
    }

    public Pair<Integer, Integer> findAmongstCells(Predicate<Cell> predicate) {
        for (int x = 0; x < 20; ++x) {
            for (int y = 0; y < 20; ++y) {
                if (predicate.test(grid[x][y]))
                    return new Pair(x, y);
            }
        }
        return null;
    }


    public Optional<Pair<Integer, Integer>> safeFindAmongstCells(Predicate<Cell> predicate) {
        for (int x = 0; x < 20; ++x) {
            for (int y = 0; y < 20; ++y) {
                if (predicate.test(grid[x][y]))
                    return Optional.of(new Pair(x, y));
            }
        }
        return Optional.empty();
    }


    private void dotoEachCell(Consumer<Cell> func) {
        for (int x = 0; x < 20; ++x) {
            for (int y = 0; y < 20; ++y) {
                func.accept(grid[x][y]);
            }
        }
    }

    @Override
    public Optional<Cell> below(Cell cell) {
        return safeFindAmongstCells((c) -> c == cell)
                .filter((pair) -> pair.first < 19)
                .map((pair) -> grid[pair.first + 1][pair.second]);


    }


    @Override
    public Optional<Cell> above(Cell cell) {
        return safeFindAmongstCells((c) -> c == cell)
                .filter((pair) -> pair.first > 0)
                .map((pair) -> grid[pair.first - 1][pair.second]);
    }

    @Override
    public Optional<Cell> rightOf(Cell cell) {
        return safeFindAmongstCells((c) -> c == cell)
                .filter((pair) -> pair.second < 19)
                .map((pair) -> grid[pair.first][pair.second + 1]);
    }

    @Override
    public Optional<Cell> leftOf(Cell cell) {
        return safeFindAmongstCells((c) -> c == cell)
                .filter((pair) -> pair.second > 0)
                .map((pair) -> grid[pair.first][pair.second - 1]);
    }

    @Override
    public List<RelativeMove> movesBetween(Cell from, Cell to, GamePiece<Cell> mover) {
        Pair<Integer, Integer> fromIndex = findAmongstCells((c) -> c == from);
        Pair<Integer, Integer> toIndex = findAmongstCells((c) -> c == to);

        List<RelativeMove> result = new ArrayList<RelativeMove>();

        //horizontal movement
        if (fromIndex.second <= toIndex.second) {
            for (int i = fromIndex.second; i < toIndex.second; i++) {
                result.add(new MoveRight(this, mover));
            }
        } else {
            for (int i = toIndex.second; i < fromIndex.second; i++) {
                result.add(new MoveLeft(this, mover));
            }
        }

        //vertical movement
        if (fromIndex.first <= toIndex.first) {
            for (int i = fromIndex.first; i < toIndex.first; i++) {
                result.add(new MoveDown(this, mover));
            }
        } else {
            for (int i = toIndex.first; i < fromIndex.first; i++) {
                result.add(new MoveUp(this, mover));
            }
        }
        return result;
    }


}
