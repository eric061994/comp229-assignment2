import bos.GamePiece;
import bos.RelativeMove;

import java.awt.*;
import java.util.Optional;
import java.util.function.Supplier;

public interface CharacterInterface extends GamePiece<Cell> {

    //Methods that will be implemented into concrete classes

    public void paint(Graphics g);
    public Cell getLocationOf();
    public void setLocationOf(Cell c);
    public RelativeMove aiMove(Stage stage);
    public void setBehaviour(Supplier<RelativeMove> behaviour);
    public int getMovesLeft();
    public void setMovesLeft(int m);
    public int getMovement();
    public Supplier<RelativeMove> getBehaviour();
    public void setMovement(int m);
    public Optional<String> getType();
    public Optional<Image> getDisplay();

}
