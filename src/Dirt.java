import java.awt.*;
import java.util.Optional;

public class Dirt extends Cell {

    //This and subsequent subclasses determine the colour of the surface
    public Dirt(int x, int y){
        super(x, y);
        terrain = Optional.of("Dirt");
    }

    @Override
    public void paint(Graphics g, Boolean highlighted) {
        g.setColor(new Color(102, 51, 0));
        super.paint(g, highlighted);
    }
}
