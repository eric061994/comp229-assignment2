import bos.GamePiece;
import bos.RelativeMove;

import java.awt.*;
import java.util.ArrayList;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * Created by 44942117 on 22/08/2017.
 */

public abstract class Character implements  CharacterInterface{
    protected Optional<Image> display;
    //type variable which will be given a value in all subsequent subclasses, allows the subclasses to be identified
    protected Optional<String> type;
    protected Cell location;
    private Supplier<RelativeMove> behaviour;
    protected int movement;
    protected int movesLeft;



    public Character(Cell location, Supplier<RelativeMove> behaviour){
        this.location = location;
        this.display = Optional.empty();
        this.type = Optional.empty();
        this.behaviour = behaviour;
        this.movement = 1;
        this.movesLeft = 1;
    }

    @Override
    public void paint(Graphics g){
        if (display.isPresent()){
            g.drawImage(display.get(), location.x + 2, location.y + 2, 31, 31, null);
        }

    }

    public Optional<Image> getDisplay(){ return display; }

    @Override
    public Cell getLocationOf(){
        return location;
    }

    @Override
    public int getMovesLeft(){ return movesLeft; }

    @Override
    public void setMovesLeft(int m){ movesLeft = m; }

    @Override
    public int getMovement(){return movement; }

    @Override
    public void setMovement(int m){ movement = m; }

    @Override
    public void setLocationOf(Cell c){
        location = c;
    }

    @Override
    public void setBehaviour(Supplier<RelativeMove> behaviour){
        this.behaviour = behaviour;
    }

    @Override
    public Supplier<RelativeMove> getBehaviour(){ return this.behaviour; }

    @Override
    public RelativeMove aiMove(Stage stage){
        return behaviour.get();
    }

    @Override
    public Optional<String> getType(){return type;}





}
