import java.awt.*;
import java.util.Optional;

public class LightGrass extends Cell {

    public LightGrass(int x, int y){
        super(x, y);
        terrain = Optional.of("Grass");
    }

    @Override
    public void paint(Graphics g, Boolean highlighted) {
        g.setColor(new Color(102, 153, 0));
        super.paint(g, highlighted);
    }
}
