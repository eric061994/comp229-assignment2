import bos.MoveRandomly;
import bos.NoMove;
import bos.RelativeMove;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.time.*;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * Created by 44942117 on 22/08/2017.
 */
public class Stage extends KeyObservable {
    protected Grid grid;
    private CharacterInterface shepherd;
    private CharacterInterface sheep;
    private CharacterInterface wolf;
    private Player player;
    private List<CharacterInterface> allCharacters;
    private Instant timeOfLastMove;


    public Stage() {

        grid = new Grid(10, 10);
        shepherd = new Shepherd(grid.getCell(0, 0), () -> new NoMove(grid, shepherd));
        sheep = new Sheep(grid.getCell(19, 0), () -> grid.movesBetween(sheep.getLocationOf(), shepherd.getLocationOf(), sheep).get(0));
        wolf = new Wolf(grid.getCell(19, 19), () -> grid.movesBetween(wolf.getLocationOf(), sheep.getLocationOf(), wolf).get(0));

        player = new Player(grid.getRandomCell());
        this.register(player);

        allCharacters = new ArrayList<CharacterInterface>();
        allCharacters.add(sheep);
        allCharacters.add(shepherd);
        allCharacters.add(wolf);
    }


    public void update() {

        if (!player.inMove()) {
            if (sheep.getLocationOf() == shepherd.getLocationOf()) {
                System.out.println("Sheep is safe");
                System.exit(0);
            } else if (sheep.getLocationOf() == wolf.getLocationOf()) {
                System.out.println("Sheep is dead");
                System.exit(1);
            } else {
                if (sheep.getLocationOf().x == sheep.getLocationOf().y) {
                    sheep.setBehaviour(() -> new NoMove(grid, sheep));
                    shepherd.setBehaviour(() -> grid.movesBetween(shepherd.getLocationOf(), sheep.getLocationOf(), shepherd).get(0));
                }

                allCharacters.forEach(c -> {
                    if(c.getMovesLeft() > 0) {
                        c.aiMove(this).perform();
                        //Safety check to ensure there isn't a nullpointerException, then determines the terrain
                        if(c.getLocationOf().terrain.isPresent()){
                            //If the character lands on a Dirt terrain, it calls the Decorator, CharacterDirt
                            if(c.getLocationOf().terrain.get() == "Dirt"){
                                c = new CharacterDirt(c);
                                //Determine which character was "decorated" then ensure the entry in allCharacters is updated
                                if(c.getType().isPresent() && c.getType().equals(Optional.of("Wolf"))){
                                    allCharacters.set(2, c);
                                } else if(c.getType().isPresent() && c.getType().equals(Optional.of("Sheep"))) {
                                    allCharacters.set(0, c);
                                }
                            }
                            //If the character lands on a Grass terrain, it calls the Decorator, CharacterGrass
                            if(c.getLocationOf().terrain.get() == "Grass") {
                                c = new CharacterGrass(c);
                                //Determine which character was "decorated" then ensure the entry in allCharacters is updated
                                if(c.getType().isPresent() && c.getType().equals(Optional.of("Wolf"))){
                                    allCharacters.set(2, c);
                                } else if(c.getType().isPresent() && c.getType().equals(Optional.of("Sheep"))) {
                                    allCharacters.set(0, c);
                                }
                            }
                        }
                        //Decrements the MovesLeft
                        c.setMovesLeft(c.getMovesLeft() - 1);
                    }
                });
                if(sheep.getMovesLeft() == 0 && wolf.getMovesLeft() == 0 && shepherd.getMovesLeft() == 0) {
                    player.startMove();
                    timeOfLastMove = Instant.now();
                    sheep.setMovesLeft(sheep.getMovement());
                    wolf.setMovesLeft(wolf.getMovement());
                    shepherd.setMovesLeft(shepherd.getMovement());
                }
            }
        }
        //Ensures all the entries are updated to the decorated class
        sheep = allCharacters.get(0);
        shepherd = allCharacters.get(1);
        wolf = allCharacters.get(2);
    }

    public void paint(Graphics g, Point mouseLocation) {
        grid.paint(g, mouseLocation);
        sheep.paint(g);
        shepherd.paint(g);
        wolf.paint(g);
        player.paint(g);
    }
}
