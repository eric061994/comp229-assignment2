import bos.RelativeMove;

import java.awt.*;
import java.util.function.Supplier;

public class CharacterDirt extends CharacterDecorator {

    CharacterInterface character;
    //Initialise counter and an array to store randomly generated locations for dot
    private int counter;
    private int[][] dotLocation = new int [10][2];

    public CharacterDirt(CharacterInterface c){
        super(c);
        character = c;
        java.util.Random rand = new java.util.Random();
        //Increment counter each time the constructor is called, indicating the concrete class has been decorated
        counter++;
        character.getLocationOf();
        //Randomly generate location for dot
        for(int i = 0; i < counter; i++) {
            this.dotLocation[counter][0] = rand.nextInt(29) + 6;
            this.dotLocation[counter][1] = rand.nextInt(29) + 6;
        }
        //decrement movement
        c.setMovement(c.getMovement() - 1);

    }


    @Override
    public void paint(Graphics g){
        super.paint(g);

        //Draws the dot, the colour white has been used for visibility
        for(int i = 0; i < counter; i++) {
            g.setColor(Color.BLACK);
            System.out.println(character.getLocationOf());
            g.fillOval(character.getLocationOf().x + dotLocation[i][0], character.getLocationOf().y + dotLocation[i][1], 8, 8);
        }

    }




}
