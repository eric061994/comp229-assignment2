import bos.MoveRandomly;
import bos.NoMove;
import bos.RelativeMove;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

public class Wolf extends Character {

    public Wolf(Cell location, Supplier<RelativeMove> behaviour){
        super(location, behaviour);
        try{
            display = Optional.of(ImageIO.read(new File("wolf.png")));
        } catch (Exception e) {
            display = Optional.empty();
        }
        movement = 4;
        movesLeft = 4;

        type = Optional.of("Wolf");
    }
}
