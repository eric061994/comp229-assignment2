import bos.RelativeMove;

import java.awt.*;
import java.util.function.Supplier;

public class CharacterGrass extends CharacterDecorator {

    CharacterInterface character;
    //Initialise counter and an array to store randomly generated locations for dot
    private int counter;
    private int[][] dotLocation = new int [10][2];

    public CharacterGrass (CharacterInterface c){
        super(c);
        character = c;
        java.util.Random rand = new java.util.Random();
        //decrement movement
        c.setMovement(c.getMovement() - 1);
        //Increment counter each time the constructor is called, indicating the concrete class has been decorated
        counter++;
        //Randomly generate location for dot
        for(int i = 0; i < counter; i++) {
            this.dotLocation[counter][0] = rand.nextInt(29) + 6;
            this.dotLocation[counter][1] = rand.nextInt(29) + 6;
        }

    }


    @Override
    public void paint(Graphics g){
        super.paint(g);

        //Draws the dot, the colour white has been used for visibility
        for(int i = 0; i < counter; i++) {
            g.setColor(Color.WHITE);
            g.fillOval(character.getLocationOf().x + dotLocation[i][0], character.getLocationOf().y + dotLocation[i][1], 8, 8);
        }

    }



}
