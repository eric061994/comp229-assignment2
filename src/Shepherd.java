import bos.MoveRandomly;
import bos.NoMove;
import bos.RelativeMove;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * Created by 44942117 on 22/08/2017.
 */
public class Shepherd extends Character {

    public Shepherd(Cell location, Supplier<RelativeMove> behaviour) {
        super(location, behaviour);
        try{
            display = Optional.of(ImageIO.read(new File("shepherd.png")));
        } catch (Exception e) {
            display = Optional.empty();
        }

        type = Optional.of("Shepherd");
    }
}



