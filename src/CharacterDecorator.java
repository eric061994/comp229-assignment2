import bos.RelativeMove;

import java.awt.*;
import java.util.Optional;
import java.util.function.Supplier;

public abstract class CharacterDecorator implements CharacterInterface {

    CharacterInterface character;

    public CharacterDecorator(CharacterInterface c){
        this.character = c;

    }

    public void paint(Graphics g){
        if (character.getDisplay().isPresent()){
            g.drawImage( character.getDisplay().get(), character.getLocationOf().x + 2, character.getLocationOf().y + 2, 31, 31, null);
        }
    }
    public Optional<Image> getDisplay(){ return character.getDisplay(); }

    public Cell getLocationOf(){
        return character.getLocationOf();
    }

    public int getMovesLeft(){ return character.getMovesLeft(); }

    public void setMovesLeft(int m){ character.setMovesLeft(m); }

    public int getMovement(){ return character.getMovement(); }

    public void setMovement(int m){ character.setMovement(m);}

    public void setLocationOf(Cell c){ character.setLocationOf(c); }

    public void setBehaviour(Supplier<RelativeMove> behaviour){
        character.setBehaviour(behaviour);
    }

    public Supplier<RelativeMove> getBehaviour(){ return character.getBehaviour(); }

    public RelativeMove aiMove(Stage stage) { return character.getBehaviour().get(); }

    public Optional<String> getType() {return character.getType(); }

}
