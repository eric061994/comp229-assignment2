import java.awt.*;
import java.util.Optional;

public class DarkTree extends Cell {

    public DarkTree(int x, int y){
        super(x, y);
        terrain = Optional.empty();
    }

    @Override
    public void paint(Graphics g, Boolean highlighted) {
        g.setColor(new Color(0, 102, 0));
        super.paint(g, highlighted);
    }
}
